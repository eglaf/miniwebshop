TestTask
-----

Install
-----
**CMD command: composer install**<br />
To create autoload.php
 
**Create database 'miniwebshop' and import db_dump.sql**<br />
Or rewrite the database name in _config/parameters.json_ file. 

**Document root: /web/**<br />
The routing module needs the .htaccess config.<br />
I used xampp for development... i'm not sure, how a Nginx server should be configured to do the same.

Urls
-----
**Site**
- http://miniwebshop
- http://miniwebshop/page/1
- http://miniwebshop/product/1

**Admin**
- http://miniwebshop/admin
- http://miniwebshop/admin/list
- http://miniwebshop/admin/create
- http://miniwebshop/admin/update/1


Files
-----
- src/MiniWebShop/Controller/
- src/MiniWebShop/Form/
- src/MiniWebShop/Service/
- src/MiniWebShop/view/


Comments
-----
Task description said i shouldn't use PHP framework.<br />
I think it meant i shouldn't use PHP framework, that was written by someone else.<br />
(I know this Egf abomination is a little chaotic. My goal wasn't to replace real frameworks... it's just an experiment from my part.)

Unfortunately i don't have the time (neither the willpower) to create the Validation or FlashMessage modules on this weekend.<br />
Validation is simply missing, FlashMessages are shown as Exceptions.
Sorry about that.


**Site**<br />
List + details... PHP

**Admin**<br />
List (search, sorting, pagination)... JS
<br />Forms... PHP  