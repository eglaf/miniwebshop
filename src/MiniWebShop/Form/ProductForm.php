<?php

namespace MiniWebShop\Form;

use Egf\Ancient;

/**
 * Class ProductForm
 */
class ProductForm extends Ancient\Form {
	
	/**
	 * Create form inputs.
	 */
	public function buildForm() {
		$this
			->addText('Product name', 'name')
			->addTextarea('Product description', 'description')
			->addCheckbox('Active', 'active')
			->addFile('Image', 'image')
			->addSubmit('Save');
	}
	
}