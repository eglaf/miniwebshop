<?php

namespace MiniWebShop\Controller;

use \Egf\Service;

use \MiniWebShop\Form\ProductForm;

/**
 * Class FormController
 * todo Repository service insert/update methods
 * todo flash messages
 */
class AdminController extends \Egf\Ancient\Controller {
	
	/** @var Service\Log */
	protected $log;
	
	/** @var Service\MyDb\MyDb|Service\MyDb\Connection */
	protected $myDb;
	
	/** @var Service\Template */
	protected $template;
	
	/** @var Service\Request */
	protected $request;
	
	/** @var \MiniWebShop\Service\FileUpload */
	protected $fileUpload;
	
	/**
	 * Init.
	 */
	public function init() {
		$this->log        = $this->getService('log');
		$this->myDb       = $this->getService('myDb');
		$this->template   = $this->getService('template');
		$this->request    = $this->getService('request');
		$this->fileUpload = $this->getService('fileUpload');
	}
	
	/**
	 * Admin index... redirects to list.
	 */
	public function indexAction() {
		$this->redirect('/admin/product/list');
	}
	
	/**
	 * List of products.
	 */
	public function listAction() {
		/** @var \mysqli_result $productsQr */
		$productsQr = $this->myDb->query('SELECT id, name, description, active, storage_file_name, original_file_name FROM product WHERE delete_date IS NULL');
		
		echo $this->template->render("MiniWebShop:Admin/list", [
			'pathToRoot' => $this->app->getPathToRoot(),
			'imageDir'   => $this->getParam('upload_file_dir'),
			'listAsJson' => json_encode($productsQr->fetch_all(MYSQLI_ASSOC)),
		]);
	}
	
	/**
	 * Show a product creation form.
	 */
	public function createAction() {
		/** @var ProductForm $form */
		$form = $this->getService('formFactory')->newForm(ProductForm::class);
		
		echo $this->template->render("MiniWebShop:Admin/form", [
			"formHtml" => $form->getAsHtml(),
		]);
	}
	
	/**
	 * Save a newly created product, then redirect to form.
	 */
	public function createSubmitAction() {
		// Check if product name is taken.
		/** @var \mysqli_result $existingProductQr */
		$existingProductQr = $this->myDb->query("SELECT id FROM product WHERE name = '{$this->getFormData('name')}';");
		if ($existingProductQr->fetch_all()) {
			throw $this->log->exception("Product with the name {$this->getFormData('name')} already exists... sadly there isn't a flash message feature yet.");
		}
		
		// Upload file.
		$requestFile = $this->request->getFile('image');
		if ( ! $requestFile['error']) {
			$this->fileUpload->uploadFile($requestFile);
			if ($this->fileUpload->getLastErrorCode()) {
				throw $this->log->exception('File upload error... ' . $this->fileUpload->getLastErrorMessage());
			}
		}
		// Problem with file uploading (except no file was uploaded)
		elseif ($requestFile['error'] !== 4) {
			throw $this->log->exception("File upload error code: {$requestFile['error']}");
		}
		
		// Save product.
		$this->myDb->query('INSERT INTO product (name, description, active, storage_file_name, original_file_name) VALUES (?, ?, ?, ?, ?)', [
			$this->getFormData('name'),
			$this->getFormData('description'),
			boolval($this->getFormData('active')),
			$this->fileUpload->getLastUploadedFileStorageName(),
			$this->fileUpload->getLastUploadedFileOriginalName(),
		]);
		
		// Redirect to list.
		$this->redirect('/admin/product/list');
	}
	
	/**
	 * Show a product update form.
	 * @param int $id
	 */
	public function updateAction($id) {
		/** @var \mysqli_result $productQr */
		$productQr = $this->myDb->query("SELECT * FROM product WHERE id = '{$id}';");
		$product   = $productQr->fetch_assoc();
		
		if ( ! $product) {
			throw $this->log->exception("Product with id:{$id} not found!");
		}
		
		/** @var ProductForm $form */
		$form = $this->getService('formFactory')->newForm(ProductForm::class);
		$form->loadValues($product);
		
		echo $this->template->render("MiniWebShop:Admin/form", [
			"formHtml" => $form->getAsHtml(),
		]);
	}
	
	/**
	 * Save updated product.
	 * @param $id int
	 */
	public function updateSubmitAction($id) {
		// Check existing product.
		/** @var \mysqli_result $existingProductQr */
		$existingProductQr = $this->myDb->query("SELECT id FROM product WHERE id != {$id} AND name = '{$this->getFormData('name')}';");
		if ($existingProductQr->fetch_all()) {
			throw $this->log->exception("Different product with the name {$this->getFormData('name')} already exists... sadly there isn't a flash message feature yet.");
		}
		
		// Updated values.
		$updateValues = 'name = ?, description = ?, active = ?';
		$updateParams = [
			$this->getFormData('name'),
			$this->getFormData('description'),
			$this->getFormData('active'),
		];
		
		// Upload file.
		$requestFile = $this->request->getFile('image');
		if ( ! $requestFile['error']) {
			$this->fileUpload->uploadFile($requestFile);
			if ($this->fileUpload->getLastErrorCode()) {
				throw $this->log->exception('File upload error... ' . $this->fileUpload->getLastErrorMessage());
			}
			// Change image in update query.
			$updateValues   .= ', storage_file_name = ?, original_file_name = ?';
			$updateParams[] = $this->fileUpload->getLastUploadedFileStorageName();
			$updateParams[] = $this->fileUpload->getLastUploadedFileOriginalName();
		}
		// Problem with file uploading (except no file was uploaded)
		elseif ($requestFile['error'] !== 4) {
			throw $this->log->exception("File upload error code: {$requestFile['error']}");
		}
		
		// Add id parameter to update query (used by the WHERE part)
		$updateParams[] = $id;
		
		// Run update query.
		$this->myDb->query("UPDATE product SET {$updateValues} WHERE id = ?", $updateParams);
		
		// Redirect to the list.
		$this->redirect('/admin/product/list');
	}
	
	/**
	 * Mark product as deleted.
	 * @param int $id
	 */
	public function deleteAction($id) {
		$this->myDb->query("UPDATE product SET delete_date = NOW() WHERE id = ?;", [$id]);
		
		$this->redirect('/admin/product/list');
	}
	
	/**
	 * Get submitted form data.
	 * @param string $key
	 * @return mixed
	 */
	protected function getFormData($key) {
		return $this->request->getRequest($key);
	}
	
}