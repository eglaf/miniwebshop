<?php

namespace MiniWebShop\Controller;

use \Egf\Ancient;

/**
 * Class ListController
 */
class SiteController extends Ancient\Controller {
	
	/**
	 * Index page... redirects to list.
	 */
	public function indexAction() {
		$this->redirect("/page/1");
	}
	
	/**
	 * List of products.
	 * @param int $page
	 */
	public function listAction($page) {
		// Get limited products.
		$limitFrom = ($page - 1) * $this->getParam('product_rows_on_page');
		$limitTo   = $this->getParam('product_rows_on_page');
		/** @var \mysqli_result $productsQr */
		$productsQr = $this->getService('myDb')->query("
			SELECT id, name, description, storage_file_name FROM product
			WHERE active = 1 AND storage_file_name IS NOT NULL AND delete_date IS NULL
			ORDER BY name LIMIT {$limitFrom}, {$limitTo}");
		
		// Redirect to first page, if there are no products.
		if ( ! $productsQr->num_rows && $page !== 1) {
			$this->redirect("/page/1");
		}
		
		// Max page.
		$productCnt = $this->getService('myDb')->query('
			SELECT COUNT(id) AS cnt FROM product
			WHERE active = 1 AND storage_file_name IS NOT NULL AND delete_date IS NULL'
		)->fetch_assoc()['cnt'];
		$maxPage    = ceil($productCnt / $this->getParam('product_rows_on_page'));
		
		// Show list.
		echo $this->getService('template')->render('MiniWebShop:Site/list', [
			'imageDir' => $this->getParam('upload_file_dir'),
			'page'     => $page,
			'products' => $productsQr->fetch_all(MYSQLI_ASSOC),
			'maxPage'  => $maxPage,
		]);
	}
	
	/**
	 * Product details.
	 * @param int $id
	 */
	public function detailsAction($id) {
		$product = $this->getService('myDb')->query('SELECT name, description, storage_file_name, original_file_name FROM product WHERE id = ?', [$id])->fetch_assoc();
		
		echo $this->getService('template')->render('MiniWebShop:Site/details', [
			'imageDir' => $this->getParam('upload_file_dir'),
			'product'  => $product,
			'backUrl'  => (isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/page/1'),
		]);
	}
	
}