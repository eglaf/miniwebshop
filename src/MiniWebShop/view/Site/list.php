<table>
    <thead>
    <th>Name</th>
    <th>Description</th>
    <th>Image</th>
    </thead>
    <tbody>
	<?php foreach ($products as $product) { ?>
        <tr>
            <td>
                <a href="/product/<?php echo $product['id']; ?>">
					<?php echo $product['name']; ?>
                </a>
            </td>
            <td>
                <?php echo $product['description']; ?>
            </td>
            <td>
                <img src="<?php echo "{$imageDir}/{$product['storage_file_name']}"; ?>" style="max-height:100px;"/>
            </td>
        </tr>
	<?php } ?>
    </tbody>
</table>

<?php if ($page > 1) { ?>
    <a href="/page/1">First</a>
    <a href="/page/<?php echo $page - 1; ?>">Previous</a>
<?php } else { ?>
    <span>First</span>
    <span>Previous</span>
<?php } ?>

<?php if ($page < $maxPage) { ?>
    <a href="/page/<?php echo $page + 1; ?>">Next</a>
    <a href="/page/<?php echo $maxPage; ?>">Last</a>
<?php } else { ?>
    <span>Next</span>
    <span>Last</span>
<?php } ?>