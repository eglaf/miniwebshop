<html>
<head>
    <script type="text/javascript" src="/js/egf/egf.js"></script>
    <script type="text/javascript" src="/js/egf/egf-template.js"></script>
    <script type="text/javascript" src="/js/egf/egf-pagination.js"></script>
    <script type="text/javascript" src="/js/egf/egf-table.js"></script>
	
	<?php require_once($pathToRoot . "/web/js-templates/egf-table-template.html"); ?>
	<?php require_once($pathToRoot . "/web/js-templates/egf-table-row-template.html"); ?>
	<?php require_once($pathToRoot . "/web/js-templates/egf-pagination-template.html"); ?>

</head>
<body>

<a href="/admin/product/create">Create product</a>

<hr/>

<div id="table-container"></div>

<script type="text/javascript">

    new Egf.Table()
        .setContainerElemId('table-container')
        .setContents(JSON.parse('<?php echo $listAsJson ?>'))
        .setConfig({
            sortByColumnKey: 1,
            sortByReversed:  false,
            rowsOnPage:      6
        })
        .setTranslations({
            globalSearchPlaceholder: 'Search'
        })
        .setColumns([{
            text:     'Id',
            property: 'id',
            search:   'string',
            sort:     true
        }, {
            text:     'Name',
            property: 'name',
            search:   'string',
            sort:     true
        }, {
            text:     'Description',
            property: 'description',
            search:   'string',
            sort:     true
        }, {
            text: 'Active',
            func: function (row) {
                return (row.active ? 'Active' : 'Inactive');
            }
        }, {
            text: 'Image',
            func: function (row) {
                var imgSrc = '<?php echo $imageDir; ?>/' + row.storage_file_name;
                
                return "<img src='" + imgSrc + "' style='max-height:50px;' />";
            }
        }, {
            text: '',
            func: function (row) {
                var url = '/admin/product/update/' + row.id;

                return "<a href='" + url + "' class='btn btn-default btn-xs'>Update</a>";
            }
        }, {
            text: '',
            func: function (row) {
                var url = '/admin/product/delete/' + row.id;

                return "<a href='" + url + "'  onclick='return confirm(\"Are you sure?\")' class='btn btn-danger btn-xs'>Delete</a>";
            }
        }])
        .init();

</script>

</body>
</html>