<?php

namespace MiniWebShop\Service;

/**
 * Class FileUpload
 */
class FileUpload extends \Egf\Ancient\Service {
	
	/** @var string */
	protected $uploadDir = '';
	
	/** @var string */
	protected $uploadedFileOriginalName = null;
	
	/** @var string */
	protected $uploadedFileStorageName = null;
	
	/** @var int */
	protected $error = 0;
	
	/** @var array */
	protected $errors = [
		1 => 'File too big!',
		2 => 'Invalid file type!',
		3 => 'Could not copy file!',
	];
	
	
	/**
	 * Initialize.
	 */
	public function init() {
		$this->uploadDir = "{$this->app->getPathToRoot()}/web/{$this->getParam('upload_file_dir')}";
		
		$this->createUploadDirIfNecessary();
	}
	
	/**
	 * Create upload directory if it doesn't exist.
	 */
	protected function createUploadDirIfNecessary() {
		if ( ! file_exists($this->uploadDir)) {
			mkdir($this->uploadDir, 0777, TRUE);
		}
	}
	
	
	/**
	 * Upload file... gives back
	 * @param array $requestFile
	 */
	public function uploadFile($requestFile) {
		$this->error = 0;
		
		// Check size.
		if ($requestFile['size'] > $this->getParam('upload_file_max_size')) {
			$this->error = 1;
		}
		
		// Check file extension and mimeType.
		$ext      = strtolower(pathinfo($requestFile['name'], PATHINFO_EXTENSION));
		$mimeType = mime_content_type($requestFile['tmp_name']);
		if ( ! in_array($ext, $this->getParam('upload_file_extensions'))
		     || ! in_array($mimeType, $this->getParam('upload_file_mime'))) {
			$this->error = 2;
		}
		
		// Copy file.
		$uniqueName = uniqid() . ".{$ext}";
		if (move_uploaded_file($requestFile["tmp_name"], "{$this->uploadDir}/{$uniqueName}")) {
			$this->uploadedFileStorageName      = $uniqueName;
			$this->uploadedFileOriginalName = $requestFile["name"];
		}
		// Cannot copy.
		else {
			$this->error = 3;
		}
	}
	
	/**
	 * Get the original name of the last uploaded file.
	 * @return string
	 */
	public function getLastUploadedFileOriginalName() {
		return $this->uploadedFileOriginalName;
	}
	
	/**
	 * Get the new name of the last uploaded file.
	 * @return string
	 */
	public function getLastUploadedFileStorageName() {
		return $this->uploadedFileStorageName;
	}
	
	/**
	 * Get the last error code.
	 * @return int
	 */
	public function getLastErrorCode() {
		return $this->error;
	}
	
	/**
	 * Get the last error message.
	 * @return string
	 */
	public function getLastErrorMessage() {
		return $this->errors[$this->error];
	}
	
	
}