<?php

namespace Egf\Service\MyDb\Helper\DbWhere;

/**
 * Class In
 */
class In extends Base {

	/**
	 * A basic condition equation string. It uses NULL-safe equal operator.
	 * @return string
	 */
	public function getConditionEquation() {
		return ' IN ';
	}

}