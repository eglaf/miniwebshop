<?php

namespace Egf\Core;

use \Egf\App;
use \Egf\Util;

/**
 * Class Router
 * @url https://marulanda.me/building-a-php-router/
 */
class Router {
	
	/** @var App */
	protected $app;
	
	/** @var string */
	protected $basePath;
	
	/** @var string */
	protected $requestUri;
	
	/** @var string */
	protected $requestMethod;
	
	/** @var array */
	protected $httpMethods = ['get', 'post', 'put', 'patch', 'delete'];
	
	/** @var array */
	protected $wildCards = ['int' => '/^[0-9]+$/', 'any' => '/^[0-9A-Za-z-]+$/'];
	
	
	/**
	 * Router constructor.
	 * @param App $app
	 */
	public function __construct($app) {
		$this->app = $app;
		
		$this->basePath      = $this->app->getConfig('base_path', '');
		$this->requestUri    = rtrim(strtok($_SERVER['REQUEST_URI'], '?'), '/');
		$this->requestMethod = $this->getRequestMethod();
	}
	
	/**
	 * Get request method... if not valid, gives back get.
	 * @return string
	 */
	protected function getRequestMethod() {
		$method = strtolower($_SERVER['REQUEST_METHOD']);
		
		if (in_array($method, $this->httpMethods)) {
			return $method;
		}
		
		return 'get';
	}
	
	/**
	 * Create and call controller action if it's the current route.
	 * @param string $method
	 * @param string $path
	 * @param string $objectName
	 * @param string $action
	 * @return bool Did Url match the route.
	 *
	 * todo Cut Action prefix, and try method without that?
	 */
	public function controllerAction($method, $path, $objectName, $action) {
		$method     = strtolower($method);
		$path       = ($path == '/' ? $this->basePath : $this->basePath . $path);
		$objectName = Util::slashing($objectName, Util::slashingAddLeft | Util::slashingBackslash);
		$matches    = $this->getWildCardVariablesIfMatching($path);
		
		// If current route.
		if (is_array($matches) && $method == $this->requestMethod) {
			// Create controller, add app object.
			$controller = new $objectName($this->app);
			
			// Check action method... if doesn't exists, checks with "Action" postfix.
			if ( ! Util::hasObjectMethod($controller, $action)) {
				$action = $action . 'Action';
			}
			if ( ! Util::hasObjectMethod($controller, $action)) {
				throw $this->app->getService('log')->exception("The Controller class '{$objectName}' misses the method '{$action}'!");
			}
			
			// Call controller action.
			Util::callObjectMethod($controller, $action, $matches);
			
			return TRUE;
		}
		
		return FALSE;
	}
	
	/**
	 * If the route is matching, gives back an array with the parameters... empty array if there weren't any.
	 * Gives back false, if the route doesn't match.
	 * @param string $sRoute
	 * @return array|false
	 */
	protected function getWildCardVariablesIfMatching($sRoute) {
		$variables = [];
		$aRequest  = explode('/', $this->requestUri);
		$aRoute    = explode('/', Util::slashing($sRoute, Util::slashingTrimRight));
		
		if (count($aRequest) == count($aRoute)) {
			// Iterate route fragments.
			foreach ($aRoute as $key => $value) {
				// Continue while the routes are matching.
				if ($value == $aRequest[$key]) {
					continue;
				}
				// Check if the route fragment is a parameter variable.
				elseif ($value[0] == '(' && substr($value, - 1) == ')') {
					$strip = str_replace(['(', ')'], '', $value);
					$exp   = explode(':', $strip);
					
					if (array_key_exists($exp[0], $this->wildCards)) {
						$pattern = $this->wildCards[$exp[0]];
						
						if (preg_match($pattern, $aRequest[$key])) {
							if (isset($exp[1])) {
								$variables[$exp[1]] = $aRequest[$key];
							}
							
							// Matching pattern.
							continue;
						}
					}
				}
				
				// Not the current route.
				return FALSE;
			}
			
			// Route matches... gives back the parameters... or an empty array.
			return $variables;
		}
		
		// Not the current route.
		return FALSE;
	}
}