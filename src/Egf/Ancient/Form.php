<?php

namespace Egf\Ancient;

use \Egf\App;

/**
 * Class Ancient Form
 *
 * todo attributes (htmlView)
 * todo getAsJson
 */
abstract class Form {
	
	/** @var App */
	protected $app;
	
	/** @var string */
	protected $action;
	
	/** @var string */
	protected $method = 'POST';
	
	/** @var array */
	protected $formInputs;
	
	/** @var boolean Add encode type multipart form data to html. */
	protected $encTypeMultipartFormData = FALSE;
	
	/** @var array */
	protected $values;
	
	
	/**
	 * Create form inputs.
	 */
	protected abstract function buildForm();
	
	
	/**
	 * Form constructor.
	 * @param App $app
	 */
	public function __construct(App $app) {
		$this->app = $app;
		
		$this->buildForm();
	}
	
	/**
	 * Set method.
	 * @param $method
	 * @return $this
	 */
	public function setMethod($method) {
		if (in_array(strtoupper($method), ['POST', 'PUT', 'PATCH', 'DELETE'])) {
			$this->method = strtoupper($method);
		}
		
		return $this;
	}
	
	/**
	 * Load default values into form.
	 * @param $values
	 * @return $this
	 */
	public function loadValues($values) {
		$this->values = $values;
		
		return $this;
	}
	
	/**
	 * Get form as html.
	 * @return string
	 */
	public function getAsHtml() {
		$result = '';
		
		$result .= $this->app->getService('template')->render('Egf:formStart', [
			'action'                   => $this->action,
			'method'                   => $this->method,
			'encTypeMultipartFormData' => $this->encTypeMultipartFormData,
		]);
		
		foreach ($this->formInputs as $input) {
			$result .= $this->app->getService('template')->render("Egf:FormTypes/{$input['type']}", [
				'label' => (isset($input['label']) ? $input['label'] : $input['name']),
				'name'  => $input['name'],
				'id'    => isset($input['attributes']['id']) ? $input['attributes']['id'] : $input['name'],
				'value' => $this->getAsHtmlValue($input),
				'checked' => ($input['type'] === 'checkbox' && isset($this->values[$input['name']]) && boolval($this->values[$input['name']])),
			]);
		}
		
		$result .= $this->app->getService('template')->render('Egf:formEnd');
		
		return $result;
	}
	
	/**
	 * Get the input value... in case of checkbox, always gives back 1.
	 * @param array $input
	 * @return string
	 */
	protected function getAsHtmlValue($input) {
		if ($input['type'] === 'checkbox') {
			return '1';
		}
		
		return (isset($this->values[$input['name']]) ? $this->values[$input['name']] : '');
	}
	
	
	
	/**************************************************************************************************************************************************************
	 *                                                          **         **         **         **         **         **         **         **         **         **
	 * Inputs                                                     **         **         **         **         **         **         **         **         **         **
	 *                                                          **         **         **         **         **         **         **         **         **         **
	 *************************************************************************************************************************************************************/
	
	/**
	 * Add a text input.
	 * @param $label
	 * @param $name
	 * @param $attributes
	 * @return $this
	 */
	public function addText($label, $name, $attributes = []) {
		$this->addInput('text', $label, $name, $attributes);
		
		return $this;
	}
	
	/**
	 * Add a textarea input.
	 * @param $label
	 * @param $name
	 * @param $attributes
	 * @return $this
	 */
	public function addTextarea($label, $name, $attributes = []) {
		$this->addInput('textarea', $label, $name, $attributes);
		
		return $this;
	}
	
	/**
	 * Add a checkbox input.
	 * @param string $label
	 * @param string $name
	 * @param array $attributes
	 * @return $this
	 */
	public function addCheckbox($label, $name, $attributes = []) {
		$this->addInput('checkbox', $label, $name, $attributes);
		
		return $this;
	}
	
	/**
	 * Add a file input
	 * @param       $label
	 * @param       $name
	 * @param array $attributes
	 * @return $this
	 */
	public function addFile($label, $name, $attributes = []) {
		$this->addInput('file', $label, $name, $attributes);
		
		$this->encTypeMultipartFormData = TRUE;
		
		return $this;
	}
	
	/**
	 * Add a submit field.
	 * @param $label
	 * @param $attributes
	 */
	public function addSubmit($label, $attributes = []) {
		$this->addInput('submit', $label, 'form-submit', $attributes);
	}
	
	/**
	 * Add input.
	 * @param $type
	 * @param $label
	 * @param $name
	 * @param $attributes
	 */
	protected function addInput($type, $label, $name, $attributes) {
		$this->formInputs[$name] = [
			'type'       => $type,
			'label'      => $label,
			'name'       => $name,
			'attributes' => $attributes,
		];
	}
	
}