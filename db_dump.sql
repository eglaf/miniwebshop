-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2018 at 02:18 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `miniwebshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `egf_session`
--

CREATE TABLE `egf_session` (
  `id` char(128) NOT NULL,
  `date` datetime NOT NULL,
  `data` text NOT NULL,
  `session_key` char(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` char(127) NOT NULL,
  `description` char(255) DEFAULT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `storage_file_name` char(127) DEFAULT NULL,
  `original_file_name` char(127) DEFAULT NULL,
  `delete_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `active`, `storage_file_name`, `original_file_name`, `delete_date`) VALUES
(1, 'lake', 'lake in Ireland...', 1, '5b38b4aa42f17.jpg', 'IMG_20161126_135706.jpg', NULL),
(2, 'rock', 'rocks...', 1, '5b38b50a5ad2b.jpg', 'IMG_20161126_111042.jpg', NULL),
(3, 'squirrel', 'dude...', 1, '5b38b546e23ec.jpg', '14125647_1998717753687823_4084661295716612287_o.jpg', NULL),
(4, 'zerg', 'abathur...', 1, '5b38b55b864ef.jpg', 'abathur.jpg', NULL),
(5, 'monkey', 'bored monkey...', 1, '5b38b5b41c555.gif', 'MonkeyBoring.gif', NULL),
(6, 'hexagons', 'cake is a lie...', 1, '5b38b5fe82728.jpg', 'red-dot.jpg', NULL),
(7, 'dog', 'good boy...', 1, '5b38b646ba617.jpg', '10255916_1493993464167240_6030771848492991657_o.jpg', NULL),
(8, 'sky', 'golden sky...', 1, '5b38b6727d443.jpg', 'Atardecer_en_el_Lago_Titicaca_by_Boristofeles.jpg', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;
